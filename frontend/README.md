# Passo a Passo

## (1) Instalar Nodejs

A versão recomenda para usar esse projeto: Nodejs 12. ~
Windows: https://nodejs.org/en/

## (2) Instalar Dependecias

Depois de instalar o Nodejs, utilizei o comando dentro da pasta frontend no terminal (Windows):
```bash
npm install
```

## (3) Executar o React

Para executar o projeto, digite o comando abaixo:
```bash
npm run start
```