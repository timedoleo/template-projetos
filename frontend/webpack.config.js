const path = require('path');

var DashboardPlugin = require("webpack-dashboard/plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');

const devMode = process.env.NODE_ENV === 'development';


const separateFileCss = false;

module.exports = {
  entry: [
    "@babel/polyfill",
    './src/index.jsx'
  ],
  output: {
    filename: 'app-assets/react/main.js',
    path: path.resolve(__dirname, 'public/dist'),
    publicPath: '/'
  },
  devServer: {
    historyApiFallback: true,
    contentBase: path.join(__dirname, 'public/dist'),
    compress: true,
    stats: 'minimal',
    port: 9090,
    open: true,
    hot: true
  },
  watchOptions: {
    poll: true,
    ignored: /node_modules/
  },
  node: {
    fs: 'empty',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        resolve: {
          extensions: [".js", ".jsx"]
        },
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              ['@babel/preset-env',
              {
                "targets": {
                  "esmodules": true
                }
              }],[
              '@babel/preset-react']]
          }
        }
      },{
        test: /\.(css|scss)$/,
        use: [
          separateFileCss
            ? {
                loader: MiniCssExtractPlugin.loader,
                options: {
                  // only enable hot in development
                  hmr: process.env.NODE_ENV === 'development',
                  // if hmr does not work, this is a forceful method.
                  reloadAll: true
                }
              }
            : { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 2
            }
          },{
            loader: 'postcss-loader',
            options: {
              modules: false,
              plugins: function() {
                return [autoprefixer];
              }
            }
          },
          {
            loader: 'sass-loader'
          }
        ]
      }
  ]
  },
  plugins: [
    new DashboardPlugin(({port:9000})),
  ]
};