'use strict';

const path = require('path');
const express = require('express');
const app = express();

app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

app.use("/app-assets", express.static(__dirname +'/dist/app-assets'));
app.use("*", function(req, resp) {
  resp.sendFile(__dirname + '/dist/index.html');
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});

module.exports = app;