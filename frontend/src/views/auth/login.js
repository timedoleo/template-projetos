import React, { Component, Fragment } from "react";

class Login extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <div>
            <section id="auth-login" className="row flexbox-container">
                <div className="col-xl-8 col-11">
                    <div className="card bg-authentication mb-0">
                        <div className="row m-0">
                            <div className="col-md-6 col-12 px-0">
                                <div className="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                                    <div className="card-header pb-1">
                                        <div className="card-title">
                                            <h4 className="text-center mb-2">Template React</h4>
                                            <h5 className="text-center mb-2">Teste Template</h5>
                                        </div>
                                    </div>
                                    <div className="card-content">
                                        <div className="card-body">
                                            <div className="d-flex flex-md-row flex-column justify-content-around">
                                                <a href="#" className="btn btn-social btn-google btn-block font-small-3 mr-md-1 mb-md-0 mb-1">
                                                    <i className="bx bxl-google font-medium-3"></i>
                                                    <span className="pl-50 d-block text-center">Google</span>
                                                </a>
                                            </div>
                                            <div className="divider">
                                                <div className="divider-text text-uppercase text-muted"><small>ou entre com seu e-mail</small>
                                                </div>
                                            </div>
                                            <form action="#">
                                                <div className="form-group mb-50">
                                                    <label className="text-bold-600" htmlFor="exampleInputEmail1">E-mail</label>
                                                    <input type="email" className="form-control" name="email" id="exampleInputEmail1" placeholder="Digite o e-mail" onChange={this.handleInputChange} />
                                                </div>
                                                <div className="form-group">
                                                    <label className="text-bold-600" htmlFor="exampleInputPassword1">Senha</label>
                                                    <input type="password" className="form-control" name="password" id="exampleInputPassword1" placeholder="Digite a senha" onChange={this.handleInputChange} />
                                                </div>
                                                <div className="form-group d-flex flex-md-row flex-column justify-content-between align-items-center">
                                                    <div className="text-left">
                                                        <div className="checkbox checkbox-sm">
                                                            <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                                                            <label className="checkboxsmall" htmlFor="exampleCheck1"><small>Lembrar o acesso</small></label>
                                                        </div>
                                                    </div>
                                                    <div className="text-right">
                                                        <a href="auth-forgot-password.html" className="card-link"><small>Esqueceu a senha?</small></a>
                                                    </div>
                                                </div>
                                                <button type="submit" className="btn btn-primary glow w-100 position-relative">Login
                                                    <i id="icon-arrow" className="bx bx-right-arrow-alt"></i>
                                                </button>
                                            </form>
                                            <hr />
                                            <div className="text-center">
                                                <small className="mr-25">Não tem conta?</small>
                                                <a href="auth-register.html"><small>Entre em contato com Operações</small></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 d-md-block d-none text-center align-self-center p-3">
                                <div className="card-content">
                                    <img className="img-fluid" src="/app-assets/images/pages/login.png" alt="branding logo" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
  }
}

export default Login;
