import React, { Component } from "react";

import { Redirect } from "react-router-dom";
import { auth } from '../../helpers/Firebase'

class Logout extends React.Component {
  constructor(props) {
      super(props);

      localStorage.removeItem('user_commissions')

      // auth.signOut().then(function() {
      // // Sign-out successful.
      // }).catch(function(error) {
      // // An error happened.
      // });
  }

  render() {
      return <Redirect to="/user/login" />
  }

}

export default Logout