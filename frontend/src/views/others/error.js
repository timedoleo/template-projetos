import React, { Component, Fragment } from "react";

class Error extends Component {
  render() {
    return (
      <Fragment>
      <div class="app-content content">
          <div class="content-overlay"></div>
          <div class="content-wrapper">
              <div class="content-header row">
              </div>
              <div class="content-body">
    
                  <section class="row flexbox-container">
                      <div class="col-xl-6 col-md-7 col-9">
                          <div class="card bg-transparent shadow-none">
                              <div class="card-content">
                                  <div class="card-body text-center">
                                      <h1 class="error-title">Página não encontrada :(</h1>
                                      <p class="pb-3">
                                          Não conseguimos encontrar a página que você estava procurando</p>
                                      <img class="img-fluid" src="https://www.pixinvent.com/demo/frest-clean-bootstrap-admin-dashboard-template/app-assets/images/pages/404.png" alt="404 error" />
                                      <a href="/user" class="btn btn-primary round glow mt-3">VOLTAR PARA LOGIN</a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </section>
              </div>
          </div>
      </div>
      </Fragment>
    );
  }
}
export default Error;
