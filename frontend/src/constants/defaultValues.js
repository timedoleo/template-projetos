// export const searchPath = "/app/pages/search";
export const servicePath = window.location.hostname == 'localhost' ? "http://127.0.0.1:8085" : "https://dashboard-backend-mjnrgxcp2a-uc.a.run.app"

let objectUser = JSON.parse(localStorage.getItem('user_commissions'))
let dataUser, tokenUser, codSeller = undefined


if(objectUser){
    dataUser = objectUser.payload.client
    codSeller = objectUser.payload.client.reference == "adm" ? "all" : objectUser.payload.client.email.split("@")[0]
    tokenUser = objectUser.token
}

export {
    dataUser,
    codSeller,
    tokenUser
}