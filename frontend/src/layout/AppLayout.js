import React, { Component, Fragment } from "react";

import HeaderNavbar from '../containers/headerNavbar';
import MainMenu from '../containers/mainMenu';
import MainFooter from '../containers/mainFooter';

export default class AppLayout extends Component {
  constructor(props){
    super(props);
  }

  componentDidMount() {

  }

  render() {
    return (
      <Fragment>

        <HeaderNavbar />
        <MainMenu />

        <div className="app-content content">
            <div className="content-overlay"></div>
            <div className="content-wrapper">
              <div className="content-header row">
              </div>
              {this.props.children}
            </div>
        </div>
        <MainFooter />

      </Fragment>
    )
  }
}