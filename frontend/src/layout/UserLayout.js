import React, { Component, Fragment } from "react";
import "../assets/css/authentication.scss"

export default class UserLayout extends Component {
    constructor(props){
        super(props);

        $("body").removeClass();
        $("body").addClass("vertical-layout vertical-menu-modern 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page  pace-done");
        $("body").attr("data-col", "1-column")
    }


    render() {
        return (
        <Fragment>
            <div className="app-content content">
                <div className="content-overlay"></div>
                <div className="content-wrapper">
                    <div className="content-header row">
                    </div>
                    <div className="content-body">
                        {this.props.children}
                    </div>
                </div>
            </div>
        </Fragment>
        )
    }
}