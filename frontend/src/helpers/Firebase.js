import firebase from 'firebase/app';

import 'firebase/auth';
import 'firebase/database';

export const firebaseConfig = {
  apiKey: "AIzaSyAcZYOQNhIXYxiNaiPgftgeXavHIVDsagI",
  authDomain: "ingaia-comercial.firebaseapp.com",
  databaseURL: "https://ingaia-comercial.firebaseio.com",
  projectId: "ingaia-comercial",
  storageBucket: "ingaia-comercial.appspot.com",
  messagingSenderId: "198435084231",
  appId: "1:198435084231:web:8e3c8aee89a678f2c9abe2",
  measurementId: "G-9D7WMNLTP1"
};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();
const uiConfigFirebase = {
    signInFlow: 'popup',
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      // firebase.auth.EmailAuthProvider.PROVIDER_ID
    ],
    callbacks: {
      signInSuccessWithAuthResult: () => false
    }
};

export {
   auth,
   uiConfigFirebase
};


 