
import { dataUser } from '../constants/defaultValues';

export const _rules = [
    {
        "team": "imob",
        "conversion": [
            {
                "percentage": 45,
                "_return": 0
            },{
                "percentage": 50,
                "_return": 3
            },{
                "percentage": 55,
                "_return": 6
            },{
                "percentage": 100,
                "_return": 10
            }    
        ]
    },{
        "team": "locacao",
        "conversion": [
            {
                "percentage": 35,
                "_return": 0
            },{
                "percentage": 40,
                "_return": 3
            },{
                "percentage": 45,
                "_return": 6
            },{
                "percentage": 45,
                "_return": 10
            }    
        ]
    }, {
        "team": "corretor",
        "conversion": [
            {
                "percentage": 25,
                "_return": 0
            },{
                "percentage": 30,
                "_return": 3
            },{
                "percentage": 35,
                "_return": 6
            },{
                "percentage": 100,
                "_return": 10
            }    
        ]
    }
]

export const calculateCommissionRange = (percentage) => {
    let result = 10

    let _result = _rules.filter((value) => {
        return value.team == dataUser.reference
    })

    _rules.filter((value) => {
        return value.team == dataUser.reference
    }).forEach(el => {
        el.conversion.forEach(values => {
            if(percentage <= values.percentage ){
                result = values._return
            }
        })
    })

    console.log("entrou 10")
    return result
};