import { useState, useEffect } from 'react';

export const getDaysInMonth = (m, y) => {
  return m===2 ? y & 3 || !(y%25) && y & 15 ? 28 : 29 : 30 + (m+(m>>3)&1);
}



export const mapOrder = (array, order, key) => {
    array.sort(function (a, b) {
      var A = a[key], B = b[key];
      if (order.indexOf(A + "") > order.indexOf(B + "")) {
        return 1;
      } else {
        return -1;
      }
    });
    return array;
  };
  
  export const dateFormat = (value) => {
      let d = new Date(value);  
      return d.getDate()  + "/" + (d.getMonth()+1) + "/" + d.getFullYear()
  }
  

  
  export const dateFormatFull = (value) => {
      let d = new Date(value);
      return d.getDate()  + "/" + (d.getMonth()+1) + "/" + d.getFullYear() + " - " +
      d.getHours() + ":" + d.getMinutes();
  }

  
  export const getDateWithFormat = () => {
    const today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
  
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    return dd + '/' + mm + '/' + yyyy;
  }
  
  export const getCurrentTime=()=>{
    const now = new Date();
    return now.getHours() + ":" + now.getMinutes()
  }
  

  export let addScriptsBody = (src) => {
    const script = document.createElement("script");

    script.src = src;
    script.async = true;

    document.body.appendChild(script);
  }

  export let numberFormat = (string,decimals=2,decimal=',',thousands='.',pre='R$ ',pos=' $') => {
    if(string != undefined && isNaN(string) == false){
      var numbers = string.toString().match(/\d+/g).join([]);
      numbers = numbers.padStart(decimals+1, "0");
      var splitNumbers = numbers.split("").reverse();
      var mask = '';
      splitNumbers.forEach(function(d,i){
          if (i == decimals) { mask = decimal + mask; }
          if (i>(decimals+1) && ((i-2)%(decimals+1))==0) { mask = thousands + mask; }
          mask = d + mask;
      });
      return pre + mask;  
    }
    return 0
  }


   export let numberFormat2 = (string,decimals=2,decimal=',',thousands='.',pre='R$ ',pos=' $') => {
     if(string != undefined && isNaN(string) == false){
        // console.log(st ring)
        let numbers = string.toString().match(/\d+/g).join([]);
        numbers = numbers.padStart(decimals+1, "0");
        let splitNumbers = numbers.split("").reverse();
        let mask = '';
        splitNumbers.forEach(function(d,i){
            if (i == decimals) { mask = decimal + mask; }
            if (i>(decimals+1) && ((i-2)%(decimals+1))==0) { mask = thousands + mask; }
            mask = d + mask;
        });
        return mask; 
     }
     return 0  
  }



  let cachedScripts = {};
  export function useScript(src) {
    // Keeping track of script loaded and error state
    const [state, setState] = useState({
      loaded: false,
      error: false
    });
  
    useEffect(
      () => {
        const onScriptLoad = () => {
          cachedScripts[src].loaded = true;
          setState({
            loaded: true,
            error: false
          });
        };
  
        const onScriptError = () => {
          // Remove it from cache, so that it can be re-attempted if someone tries to load it again
          delete cachedScripts[src];
  
          setState({
            loaded: true,
            error: true
          });
        };
  
        let scriptLoader = cachedScripts[src];
        if(scriptLoader) { // Loading was attempted earlier
          if(scriptLoader.loaded) { // Script was successfully loaded
            setState({
              loaded: true,
              error: false
            });
          } else { //Script is still loading
            let script = scriptLoader.script;
            script.addEventListener('load', onScriptLoad);
            script.addEventListener('error', onScriptError);
            return () => {
              script.removeEventListener('load', onScriptLoad);
              script.removeEventListener('error', onScriptError);
            };
          }
        } else {
          // Create script
          let script = document.createElement('script');
          script.src = src;
          script.async = true;
  
          // Script event listener callbacks for load and error
  
  
          script.addEventListener('load', onScriptLoad);
          script.addEventListener('error', onScriptError);
  
          // Add script to document body
          document.body.appendChild(script);
  
          cachedScripts[src] = {loaded:false, script};
  
          // Remove event listeners on cleanup
          return () => {
            script.removeEventListener('load', onScriptLoad);
            script.removeEventListener('error', onScriptError);
          };
        }
      },
      [src] // Only re-run effect if script src changes
    );
  
    return [state.loaded, state.error];
  }