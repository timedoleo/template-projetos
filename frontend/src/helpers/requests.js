import { servicePath, tokenUser }  from '../constants/defaultValues'

let headers = {
    'Accept': 'application/json, text/plain, */*',
    'Content-Type': 'application/json',
    'Authorization': tokenUser
}


const requestPromise = (_url, _data, _type) => new Promise((resolve, reject) => {  
    let _config =  {
        method: _type,
        headers: headers        
    }

    if(_type == 'POST'){
        _config['body'] = JSON.stringify(_data)
    }

    fetch(_url,_config).then(
        response=>response.text()
    ).then(data=>{
       resolve(JSON.parse(data))   
    }).catch((erro) => {
        console.log(erro)
        reject('Erro')
    }) 
});  


export const dependencies = async (url, data, header=false, type='POST', mod=true) => {  
    let _url = url
    if(header) {
        for (const [key, value] of Object.entries(header)) {
            // console.log(key, value);
            headers[key] = value
        }

        // headers['Authorization'] = header
    }
    if(mod){
        _url = servicePath + url
    }     
    return await requestPromise(_url, data, type);
};  

/* -------------------------------------------------- */

const requestAll = (_url, _data) => new Promise((resolve, reject) => {  
    fetch(_url, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(_data)
    }).then(
        response=>response.text()
    ).then(data=>{
       resolve(JSON.parse(data))   
    }).catch((erro) => {
        reject(erro)
    }) 
});  


export const post = async (url, data) => {  
    return await requestAll(servicePath + url, data);
};  

export const timeout = (ms, promise) => {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        // reject(new Error("timeout"))
        reject("timeout")
      }, ms)
      promise.then(resolve, reject)
    })
}