import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'


const MySwal = withReactContent(Swal)


export let validEmail = (_email) => {
    if (_email != null && _email.replace(/\s+/g, "") != "") {
        var _emailMatch = _email.match("[A-Za-z0-9\\._-]+@[A-Za-z0-9\\-]+\\.[A-Za-z]+");
        if (_emailMatch != null && _emailMatch.length > 0) {
            return true;
        }
    }
    return false
}

export let isNullOrEmpty = (valor) => {
    return (valor == null || valor.toString().replace(/\s+/g, "") == "");
}

export let alertProblems = (erro, txt='Estamos com alguns problemas, por favor tente novamente!') => {
    console.error(erro)
    MySwal.fire({
        icon: 'error',
        title: 'Ops...',
        text: txt
    })
}

export let formatUrlTrial = (trial) => {
    let array = trial.split('/')
    if(array[array.length-1] == 'financeiro'){
        return 'financeiro'
    }else{
        return 'locacao'
    }
}
