import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";

import { servicePath }  from './constants/defaultValues'
import { auth } from './helpers/Firebase'

import User from './views/auth/';
import main from './views/index';
import error from './views/others/error';


class Main extends Component {
  render() {

    let loginUser = false
    let dataUser = localStorage.getItem('user_commissions')
    if(dataUser){
      loginUser = true
    }
    return (
      <React.Fragment>
        <Router>
          <Switch>
            {/* <AuthRoute path="/app" authUser={loginUser} component={app} /> */}
            <Route path="/user" render={(routeProps) => (
                <User {...routeProps} />
            )} />
            <Route path="/" exact component={main} />
            <Route path="/error" exact component={error} />
            <Redirect to="/error" />
          </Switch>
        </Router>
      </React.Fragment>
    );
  }
}

export default Main;

const AuthRoute = ({ component: Component, authUser, ...rest }) => (
  <Route
    {...rest}
    render={props =>{
        auth.signOut();
        let tokenUser = JSON.parse(localStorage.getItem('user_commissions'));

        // if(tokenUser){
        //   fetch(servicePath + '/api/controller/security/token/decode', {
        //     method: 'POST',
        //     headers: {
        //       'Accept': 'application/json, text/plain, */*',
        //       'Content-Type': 'application/json'
        //     },
        //     body: JSON.stringify(tokenUser)
        //   }).then(data=>{
        //     if(data.status == 400){
        //       localStorage.removeItem('user_commissions')
        //       window.location = '/user/login'
        //     }
        //   }).catch(() => {
        //     localStorage.removeItem('user_commissions')
        //     window.location = '/user/login'
        //   });  
        // }else{
        //   localStorage.removeItem('user_commissions')
        //   window.location = '/user/login'
        // }

        if(authUser){
          return <Component {...props} />
        }else{
          return <Redirect
            to={{
              pathname: "/user/login",
              state: { from: props.location }
            }}
          />
        }
      }     
    }
  />
);