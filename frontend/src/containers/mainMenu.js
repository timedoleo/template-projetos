import React, { Component, Fragment } from "react";
import { Link } from 'react-router-dom';

import { dataUser } from "../constants/defaultValues";

export default class MainMenu extends Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
        <div className="main-menu menu-fixed menu-light menu-accordion menu-shadow menu-dark" data-scroll-to-active="true">
            <div className="navbar-header">
                <ul className="nav navbar-nav flex-row">
                    <li className="nav-item mr-auto">
             
                    </li>
                    <li className="nav-item nav-toggle">
                        <a className="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                            <i className="bx bx-x d-block d-xl-none font-medium-4 primary"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div className="shadow-bottom"></div>
            <div className="main-menu-content">
                <ul className="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
                    <li className=" active nav-item">
                        <a href={`/${this.state.url_prefix}/dashboard/default`}>
                            <i className="menu-livicon" data-icon="desktop"></i>
                            <span className="menu-title" data-i18n="Dashboard">Dashboard</span>
                        </a>
                    </li>
                    <li className=" navigation-header"><span>Menu</span>
                    </li>
                    <li className=" nav-item">
                        <Link to={`/${this.state.url_prefix}/commissions/list`}>
                            <i className="menu-livicon" data-icon="notebook"></i>
                            <span className="menu-title" data-i18n="Invoice">Relatório de Contratos</span>
                        </Link>
                    </li>
                    <li className=" nav-item">
                        <Link to={`/${this.state.url_prefix}/bootcamp/list`}>
                            <i className="menu-livicon" data-icon="notebook"></i>
                            <span className="menu-title" data-i18n="Invoice">Relatório Bootcamp</span>
                        </Link>
                    </li>

                    <li className=" nav-item">
                        <a href="#">
                            <i className="menu-livicon" data-icon="notebook"></i>
                            <span className="menu-title" data-i18n="Invoice">Apresentações ></span>
                        </a>
                        <ul className="menu-content">
                            <li>
                                <Link to={`/${this.state.url_prefix}/presentations/list`}>
                                    <i className="bx bx-right-arrow-alt"></i>
                                    <span className="menu-item" data-i18n="Invoice List">Concluídas</span>
                                </Link>

                            </li>
                            <li>
                                <Link to={`/${this.state.url_prefix}/presentations/scheduled/list`}>
                                    <i className="bx bx-right-arrow-alt"></i>
                                    <span className="menu-item" data-i18n="Invoice List">Agendadas</span>
                                </Link>
                            </li>
                            <li>
                                <Link to={`/${this.state.url_prefix}/presentations/cancellation/list`}>
                                    <i className="bx bx-right-arrow-alt"></i>
                                    <span className="menu-item" data-i18n="Invoice List">Canceladas</span>
                                </Link>
                            </li>
                        </ul>
                    </li>

                    <li className=" nav-item">
                        <Link to="/common/pending-first-payment/list">
                            <i className="menu-livicon" data-icon="calendar"></i>
                            <span className="menu-title" data-i18n="Calendar">Pagamento Pendente</span>
                        </Link>
                    </li>


                    <li className=" nav-item">
                        <a href="#">
                            <i className="menu-livicon" data-icon="question-alt"></i>
                            <span className="menu-title" data-i18n="FAQ">Vendas sem Consultor ></span>
                        </a>
                        <ul className="menu-content">
                            {/* <li>
                                <Link to={`/common/consultant-without/contract/list`}>
                                    <i className="bx bx-right-arrow-alt"></i>
                                    <span className="menu-item" data-i18n="Invoice List">Contratos</span>
                                </Link>

                            </li>
                            <li>
                                <Link to={`/common/consultant-without/bootcamp/list`}>
                                    <i className="bx bx-right-arrow-alt"></i>
                                    <span className="menu-item" data-i18n="Invoice List">Bootcamp</span>
                                </Link>
                            </li> */}
                            <li>
                                <Link to={`/common/consultant-without/contract-within-time/list`}>
                                    <i className="bx bx-right-arrow-alt"></i>
                                    <span className="menu-item" data-i18n="Invoice List">Contratos 12 meses</span>
                                </Link>

                            </li>
                        </ul>
                    </li>
                {/* 
                    {dataUser.codSeller == "all" &&
                        <li className=" nav-item">
                            <a href="#">
                                <i className="menu-livicon" data-icon="wrench"></i>
                                <span className="menu-title" data-i18n="Account Settings">Operações</span>
                            </a>
                        </li>
                    } */}


                </ul>
            </div>
        </div>
    )
  }
}