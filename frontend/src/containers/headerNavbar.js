import React, { Component, Fragment } from "react";
import { Link } from 'react-router-dom';

import { dataUser } from "../constants/defaultValues";

export default class HeaderNavbar extends Component {
  constructor(props){
    super(props);
  }

  componentDidMount() {

  }
  render() {
    return (
        <Fragment>
            <div className="header-navbar-shadow"></div>
            <nav className="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top  navbar-dark" style={{"left": "0px"}}>
                <div className="navbar-wrapper">
                    <div className="navbar-container content">
                        <div className="navbar-collapse" id="navbar-mobile">
                            <div className="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                                <ul className="nav navbar-nav">
                                    <li className="nav-item mobile-menu d-xl-none mr-auto"><a className="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                                        <i className="ficon bx bx-menu"></i></a>
                                    </li>
                                </ul>
                                <ul className="nav navbar-nav">
                                    <li className="nav-item d-none d-lg-block"><a className="nav-link bookmark-star"><i className="ficon bx bx-star warning"></i></a>
                                        <div className="bookmark-input search-input">
                                            <div className="bookmark-input-icon"><i className="bx bx-search primary"></i></div>
                                            <input className="form-control input" type="text" placeholder="Explore Frest..." tabIndex="0" data-search="template-list" />
                                            <ul className="search-list"></ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <ul className="nav navbar-nav float-right">
                                <li className="dropdown dropdown-notification nav-item">
                                    <a className="nav-link nav-link-label" href="#" data-toggle="dropdown">
                                        <i className="ficon bx bx-bell bx-tada bx-flip-horizontal"></i>
                                        {/* <span className="badge badge-pill badge-danger badge-up">5</span> */}
                                    </a>
                                    <ul className="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                      
                                    </ul>
                                </li>
                                <li className="dropdown dropdown-user nav-item">
                                    <a className="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                        <div className="user-nav d-sm-flex d-none">
                                            <span className="user-name">{dataUser.name}</span>
                                            <span className="user-status text-muted">Online</span>
                                        </div>
                                        <span>
                                            <img className="round" src="/app-assets/images/portrait/small/avatar.png" alt="avatar" height="40" width="40" />
                                        </span>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right pb-0">
                                        <a className="dropdown-item" target="_blank" href="https://myaccount.google.com/?utm_source=OGB&tab=wk&authuser=0&utm_medium=act">
                                            <i className="bx bx-user mr-50"></i> Editar Perfil
                                        </a>
                                        <div className="dropdown-divider mb-0"></div>
                                        <Link className="dropdown-item" to="/user/logout">
                                            <i className="bx bx-power-off mr-50"></i> Logout
                                        </Link>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </Fragment>
    )
  }
}