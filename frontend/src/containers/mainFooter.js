import React, { Component, Fragment } from "react";

export default class MainMenu extends Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
        <Fragment>
          <div className="sidenav-overlay"></div>
          <div className="drag-target"></div>

          <footer className="footer footer-static footer-light">
              <p className="clearfix mb-0">
                <span className="float-left d-inline-block">2020 &copy; Operações</span>
                <span className="float-right d-sm-inline-block d-none">
                  <a className="text-uppercase" href="#" target="_blank">Ingaia</a>
                </span>
              </p>
          </footer>
        </Fragment>
    )
  }
}