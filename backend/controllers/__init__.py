from flask import Blueprint

def start_controller(module_name):
    return Blueprint(module_name.split('.')[1], module_name)

from controllers import (
    test,
)
