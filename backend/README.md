# Passo a Passo

## (1) Instalar Python

A versão recomenda para usar esse projeto: Python 3.7.6
Windows: https://www.python.org/downloads/release/python-376/

## (2) Instalar Dependecias

Depois de instalar o Python, utilize o comando dentro da pasta backend no terminal (Windows):
```bash
python -m pip install -r requirements.txt
```

## (3) Executar o Python

Para executar o projeto, digite o comando abaixo:
```bash
python main.py
```