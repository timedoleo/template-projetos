from common.mongodb import Mongodb
from common.date import date_now

import json
import datetime

from dateutil.tz import tzutc

def default(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()

class Super():
    def __init__(self):
        super().__init__()


    def all(self, database, collection):
        _result = Mongodb(database).find_all(collection)
        for el in _result:
            if '_id' in el: del el['_id']
        _result = json.dumps(_result,sort_keys=True,indent=1,default=default)
        return json.loads(_result)


    def find_alt(self, query, database, collection, limit, sort_reference):
        _result = Mongodb(database).find_alt(query, collection, limit, sort_reference)
        for el in _result:
            if '_id' in el: del el['_id']
        _result = json.dumps(_result,sort_keys=True,indent=1,default=default)
        return json.loads(_result)


    def select_one(self, query, database, collection):
        _result = Mongodb(database).find_one(query, collection)
        if _result:
            if '_id' in _result: del _result['_id']
        _result = json.dumps(_result,sort_keys=True,indent=1,default=default)
        return json.loads(_result)


    def select(self, query, database, collection):
        _result = Mongodb(database).find(query, collection)
        for el in _result:
            if '_id' in el: del el['_id']
        _result = json.dumps(_result,sort_keys=True,indent=1,default=default)
        return json.loads(_result)


    def insert_basic(self, data, name_document, collection, database, _type="$set"):
        return Mongodb(database).update_set({'name_document': name_document}, data, collection, _type)


    def insert(self, data, name_document, collection, database):
        return Mongodb(database).update_set(name_document, data, collection)


    def insert_structure(self, data, reference, name_document, collection, database):
        return Mongodb(database).update_set({'name_document': name_document}, {
            'data': data,
            'timestamp': date_now,
            'reference': reference,
            'name_document': name_document
        }, collection)


    def remove(self, name_document, collection, database):
        return Mongodb(database).delete({'name_document': name_document}, collection)


    def count(self, query, database, collection):
        return Mongodb(database).count_document(query, collection)
