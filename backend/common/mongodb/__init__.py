import pymongo
import urllib.parse
import os 

class Mongodb():
    def __init__(self, database):
        user = os.getenv("MONGO_USER")
        password = os.getenv("MONGO_PASS")

        self.client = pymongo.MongoClient(f"mongodb://{user}:{password}@comissoes-shard-00-00-iqhbg.gcp.mongodb.net:27017,comissoes-shard-00-01-iqhbg.gcp.mongodb.net:27017,comissoes-shard-00-02-iqhbg.gcp.mongodb.net:27017/test?ssl=true&replicaSet=Comissoes-shard-0&authSource=admin&retryWrites=true&w=majority") # ATLAS V2
        self.db = self.client[database]


    def find_one(self, query, path):
        _result = self.db[path].find_one(query)
        self.client.close()
        return _result


    def find(self, query, path):
        array = []
        for el in self.db[path].find(query):
            array.append(el)
        self.client.close()
        return array


    def find_all(self , path):
        array = []
        for el in self.db[path].find():
            array.append(el)

        self.client.close()
        return array


    def find_alt(self, query, path, _limit=1000, reference=''):
        array = []
        for el in self.db[path].find(query).limit(_limit).sort(reference, pymongo.DESCENDING):
            array.append(el)
        self.client.close()
        return array
        

    def insert_id(self, json, path):
        _result = self.db[path].insert_one(json).inserted_id
        self.client.close()
        return _result


    def insert_many(self, json , path):
        _result = self.db[path].insert_many(json)  
        self.client.close()
        return _result


    def update(self, query, update, path):
        _result = self.db[path].update(query, update)
        self.client.close()
        return _result


    def update_set(self, query, update, path, _type="$set"):
        _result = self.db[path].update(query, { _type: update }, upsert=True)
        self.client.close()
        return _result


    def update_one(self, query, update, path):
        _result = self.db[path].update(query, update, upsert=True)
        self.client.close()
        return _result


    def delete(self, json, path):
        _result = self.db[path].delete_many(json)
        self.client.close()
        return _result


    def count_document(self, query, path):
        _result = self.db[path].find(query).count()
        self.client.close()
        return _result