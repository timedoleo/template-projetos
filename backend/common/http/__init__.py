
from flask import Flask, render_template
from flask_socketio import SocketIO, emit
from flask_cors import CORS, cross_origin

def flask_init(name):
    return Flask(name)

def cors_init(app):
    return CORS(app)

def socketio_init(app):
    return SocketIO(app)