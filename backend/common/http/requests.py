
from flask import Flask, jsonify, render_template

from common.constants import service_path

import json
import requests

headers = {
    'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X)' + 
    'AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1',
    'Content-Type': 'application/json'
}

class Call():

    @staticmethod
    def convert(_req):
        if type(_req.get_json()) is str:
            return json.loads(_req.get_json())
        else:
            return _req.get_json()


    @staticmethod
    def post(_url, _json):
        return requests.post(_url, json=json.dumps(_json), headers=headers)


    @staticmethod
    def post_data(_url, _json, add_headers=None):
        if add_headers:
            for el in list(add_headers):
                headers[el] = add_headers[el]

        return requests.post(_url, data=json.dumps(_json), headers=headers)

    @staticmethod
    def put_data(_url, _json, add_headers=None):
        if add_headers:
            for el in list(add_headers):
                headers[el] = add_headers[el]

        return requests.put(_url, data=json.dumps(_json), headers=headers)

    @staticmethod
    def get(_url, add_headers=None):
        if add_headers:
            for el in list(add_headers):
                headers[el] = add_headers[el]
        return requests.get(_url, headers=headers)


    @staticmethod
    def build_response(response, status_code=200):
        return jsonify(response), status_code


    @staticmethod
    def build_template(template, params):
        return render_template(template)


    @staticmethod
    def no_content_response():
        return '', 204