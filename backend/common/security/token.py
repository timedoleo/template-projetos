
from common.http.requests import Call

from flask import request
from jose import jwt
from time import time

def valid_token(token):
    try:
        decode = jwt.decode(token, 'JEIyP5RcU1', algorithms=['HS256'])
        return decode
    except jwt.ExpiredSignatureError:
        return 'Seu token esta expirado.', 400
    except:
        return 'Token Inválido.', 400
    else:
       return 'Erro', 400


def create_token(_uid, _client):
    payload = {
        'uid': _uid,
        'client': _client,
        'exp': int(time()) + 7200
    }
    token = jwt.encode(payload, 'JEIyP5RcU1', algorithm='HS256')

    return token, payload
