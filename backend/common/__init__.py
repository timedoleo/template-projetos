from datetime import datetime
import hashlib

import time
import random


def raiseValue(text):
    raise ValueError(text)


def isBlank(myString):
    return not (myString and myString.strip())


def isNotBlank(myString):
    return bool(myString and myString.strip())


def encode_string(string):
    if string is not None:
        return hashlib.md5(string.encode()).hexdigest()
    return None


def in_dictlist(key, value, my_dictlist):
    for this in my_dictlist:
        if this[key] == value:
            return this
    return {}


def new_id():
    time.sleep(0.000001)
    return time.time()
