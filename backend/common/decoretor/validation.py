from flask import request

from common.security.token import valid_token

from functools import wraps
import functools

def validation():
    def decorator(f):
        @wraps(f)
        def wrapped(*args, **kwargs):

            try:
                _request = dict(request.headers)
                _request = _request['Authorization']
                result = valid_token(_request)
            except Exception as e:
                return {"Erro":  __name__, "Mensagem": str(e)}, 400

            return f(*args, **kwargs)
        return wrapped
    return decorator