


import sqlalchemy
import os

db_user = os.environ.get("DB_USER")
db_pass = os.environ.get("DB_PASS")
db_name = os.environ.get("DB_NAME")

def create_db_connection():
    """
    esta função cria uma conexão com o banco de dados
    :return: engine e connection, usado para se conectar ao banco de dados via sqlalchemy
    """
    try:
        engine = sqlalchemy.create_engine(
                f'postgresql+psycopg2:'
                f'//{db_user}:{db_pass}'
                f'@35.225.172.100:5432'
                f'/{db_name}'
            )
        connection = engine.connect()
    except Exception as e:
        raise Exception(f'Could not connect to database - error: {e}')

    return engine, connection


def insert_into(_dirc):
    engine, connection = create_db_connection()

    placeholder = ", ".join(["%s"] * len(_dirc))
    stmt = "insert into `{table}` ({columns}) values ({values});".format(table="table_name", columns=",".join(_dirc.keys()), values=placeholder)
    # print(stmt)
    connection.execute(stmt, list(_dirc.values()))

    connection.close()
    engine.dispose()

