from datetime import date, datetime, timedelta
from dateutil import parser

import pytz
import time
import datetime

to_zone = pytz.timezone('America/Sao_Paulo')
date_now = datetime.datetime.now(to_zone)
date_now_month = f"{date_now.year}-{date_now.month}-{date_now.day} 00:00:00"

def convert_date(_date):
    return parser.parse(_date)


def calc_date(_date):
    return date_now - _date


def calcule_date_month(_date_request):
    _date_formatter = convert_date(_date_request)
    
    last_day = last_day_of_month_mod(_date_formatter) 
    first_day = convert_date(f"{_date_formatter.year}-{_date_formatter.month}-01 00:00:00")

    return {
        "$gte": first_day,
        "$lt": last_day
    }


def last_day_of_month_mod(_date_formatter):
    _day = last_day_of_month(_date_formatter)
    return convert_date(f"{_day.year}-{_day.month}-{_day.day} 23:59:59")


def last_day_of_month(date):
    if date.month == 12:
        return date.replace(day=31)
    return date.replace(month=date.month+1, day=1) - datetime.timedelta(days=1)


def parse_datetime(date_to_convert):
    pattern = "%Y-%m-%d %I:%M:%S"
    return datetime.datetime.strptime(date_to_convert,pattern)


def last_month():
    first = date_now.replace(day=1)
    lastMonth = first - datetime.timedelta(days=1)

    first_day_month = lastMonth.replace(day=1)
    last_day_month = lastMonth

    first_day_month = str(first_day_month.strftime("%Y-%m-%d")) + timePerson
    last_day_month = str(last_day_month.strftime("%Y-%m-%d")) + timePerson

    #{'created_at': {'from': '2019-08-01 00:00:00', 'to': '2019-08-31 00:00:00'}}
    return {'created_at':{'from': first_day_month,'to': last_day_month}}


def predecessor_month():
    first = date_now.replace(day=1)
    lastMonth = first - datetime.timedelta(days=1)

    first = lastMonth.replace(day=1)
    lastMonth = first - datetime.timedelta(days=1)

    first_day_month = lastMonth.replace(day=1)
    last_day_month = lastMonth

    first_day_month = str(first_day_month.strftime("%Y-%m-%d")) + timePerson
    last_day_month = str(last_day_month.strftime("%Y-%m-%d")) + timePerson

    # {'created_at': {'from': '2019-07-01 00:00:00', 'to': '2019-07-31 00:00:00'}}
    return {'created_at':{'from': first_day_month,'to': last_day_month}}

