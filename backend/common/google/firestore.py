from google.cloud import firestore

class Firestore():
    def __init__(self):
        self.__db = firestore.Client()

    def insert(self, data, name_document, *args):
        return self.__db.collection('/'.join(args)).document(name_document).set(data)


    def update(self, data, name_document, *args):
        return self.__db.collection('/'.join(args)).document(name_document).update(data)


    def select(self, key, value,  *args):
        response = self.__db.collection('/'.join(args)).where(key, '==', value)

        data = []
        for el in response.stream():
          data.append(el.to_dict())
        return data

    
    def select_all(self, *args):
        response = self.__db.collection('/'.join(args) )
        data = []
        for el in response.stream():
          data.append(el.to_dict())
          
        return data


    def select_all_id(self, *args):
        response = self.__db.collection('/'.join(args) )
        data = []
        for el in response.stream():
          data.append({
              'id': el.id,
              'obj': el.to_dict()
            })
          
        return data


    def delete(self, name_document, *args):
        return self.__db.collection('/'.join(args)).document(name_document).delete()
