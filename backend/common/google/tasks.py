from google.cloud import tasks_v2beta3
from datetime import timedelta
from google.protobuf import timestamp_pb2

import datetime
from common.constants import service_path, name_project
import json

def create_tasks(_url, _object, _task, _seconds=None):
    client = tasks_v2beta3.CloudTasksClient()
    parent = client.queue_path(name_project, 'southamerica-east1', _task)

    task = {
        'http_request': {
            'http_method': 'POST',
            'url': f'{service_path}{_url}',
            'body': json.dumps(_object).encode('utf-8')
        }
    }

    if _seconds:
        d = datetime.datetime.utcnow() + datetime.timedelta(seconds=_seconds)
        timestamp = timestamp_pb2.Timestamp()
        timestamp.FromDatetime(d)
        task['schedule_time'] = timestamp

    client.create_task(parent, task)