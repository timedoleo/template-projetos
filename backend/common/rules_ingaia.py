product = {
    'imob': '42',
    'corretor': '40',
    'locacao': '36'
}

list_services_bootcamp = [
    {"id": "181", "licenca": "42"},
    {"id": "182", "licenca": "42"},
    {"id": "183", "licenca": "42"},
    {"id": "184", "licenca": "42"},
    {"id": "201", "licenca": "42"},
    {"id": "202", "licenca": "36"},
    {"id": "203", "licenca": "36"},
    {"id": "204", "licenca": "36"},
    {"id": "214", "licenca": "40"}
]

def license_by_flat_id(_id):
    for el in list_services_bootcamp:
        if el['id'] == _id:
            return el['licenca']
    return None



rules = [
    {
        "team": "imob",
        "conversion": [
            {
                "percentage": 45,
                "_return": 0
            },{
                "percentage": 50,
                "_return": 3
            },{
                "percentage": 55,
                "_return": 6
            },{
                "percentage": 100,
                "_return": 10
            }    
        ]
    },{
        "team": "locacao",
        "conversion": [
            {
                "percentage": 35,
                "_return": 0
            },{
                "percentage": 40,
                "_return": 3
            },{
                "percentage": 45,
                "_return": 6
            },{
                "percentage": 45,
                "_return": 10
            }    
        ]
    }, {
        "team": "corretor",
        "conversion": [
            {
                "percentage": 25,
                "_return": 0
            },{
                "percentage": 30,
                "_return": 3
            },{
                "percentage": 35,
                "_return": 6
            },{
                "percentage": 100,
                "_return": 10
            }    
        ]
    }
]

		