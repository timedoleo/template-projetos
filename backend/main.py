from common.security import *
from common.http import flask_init, socketio_init, cors_init
from common.constants import config_init

import controllers

app = flask_init(__name__)

config_init['app'] = app
config_init['cors'] = cors_init(app)

for module in [k for (k, v) in sys.modules.items()
    if 'controllers.' in k and v is not None]:
        app.register_blueprint(eval('%s.api' % module))


@app.route('/')
def index():
    return '- Template Backend -'


if __name__ == '__main__':
    config_init['app'].run(debug=True,host='127.0.0.1',port=int(os.environ.get('PORT', 8085)))
